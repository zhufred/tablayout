# Tablayout

#### 介绍

在Tablayout的基础上添加字体或图标的缩放及颜色过渡动画等。

使用的Tablayout的版本：com.google.android.material:material:1.2.1

##### 实现效果如下：

<img src="demo.gif" alt="动画效果" style="zoom:50%;" />



#### 实现功能：

1.修改Indicator动画

2.添加文字缩放及颜色过渡动画

3.添加自定义图标大小及缩放和颜色过渡动画

4.添加背景颜色过渡动画

#### 用法

```xml
<com.zyk.tabs.TabLayout
    android:id="@+id/activity_test_tabs_tl"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:tabMode="scrollable"
    app:tabSelectedTextColor="#2b952b"
    app:tabTextColor="#D5FA3030"
    app:tabIndicatorColor="@color/backgroundColorDark"
    app:tabSelectedTextSize="26sp"
    app:tabTextSize="18sp"
    app:tabIconTint="#D5FA3030"
    app:tabSelectedIconTint="@color/backgroundColorDark"
    app:tabInlineLabel="true"
    app:tabIconWidth="18dp"
    app:tabIconHeight="18dp"
    app:tabIconScale="1.5"
    app:tabIndicatorFullWidth="false"
    app:tabBackground="@drawable/tab_background"
    app:tabItemBackground="#AFAEAE"
    app:tabItemSelectedBackground="#ffffff"/>
```